package redacted
{
	
	/**
	 * This class contains the data for creating a character.
	 * Calling the method CreateCharacter will return an object with the variables for character usage.
	 * @author Sausage Roll Studios
	 */
	public class Character
	{
		internal static function CreateCharacter(player:Object, units:Array, entity:int):Object
		{
			var character:Object = new Object();
			// The units values
			character.unit = units[entity];
			// The characters location
			character.characterLocation = new Array(player.baseLocation[0], player.baseLocation[1]);
			// The time of last attack
			character.characterCD = 0;
			// The characters current hp
			character.characterHP = character.unit.unitMaxHP;
			// The current animator of the character
			character.animator = units[entity].moveAnimator.Clone();
			return character;
		}
	}

}