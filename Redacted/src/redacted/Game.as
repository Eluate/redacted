package redacted
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	import redacted.UI;
	import flash.text.engine.*;
	import flash.events.MouseEvent;
	
	/**
	 * Contains the main logic and control loop.
	 * @author Sausage Roll Studios
	 */
	public class Game extends MovieClip
	{
		// Timing Variables
		internal var deltaTime:Number;
		internal var lastFrame:int;
		// Instance Variables
		internal var ui:UI;
		// Arrays
		internal var players:Array = new Array(new Object(), new Object());
		internal var units:Array = new Array();
		// Option Variables
		internal var difficultyModifier:Number = 1;
		internal var cameraPosition:Number = 0;
		internal var cameraView:Rectangle = new Rectangle(0, 0, 1024, 720);
		
		public function Start():void
		{
			// Initialize ui object
			ui = new UI();
			ui.game = this;
			// Open Menu
			ui.MenuUI(new MouseEvent(""));
			// Allow ui to be rendered
			addChild(ui);
			scrollRect = cameraView;
		}
		
		/*
		 *  Initializes all the variables required for the game then starts the game loop
		 */
		private function InitializeGame():void
		{
			// Set initial gold for both players
			players[0].gold = 50;
			players[1].gold = 50 * difficultyModifier;
			// Set base hp
			players[0].baseHP = 200;
			players[1].baseHP = 200 * difficultyModifier;
			players[0].baseMaxHP = 200;
			players[1].baseMaxHP = 200 * difficultyModifier;
			// Set xp
			players[0].xp = 0;
			players[1].xp = 0;
			// Set base location
			players[0].baseLocation = new Array(300, 0);
			players[1].baseLocation = new Array(3200, 0);
			// Retrieve unit data
			players[0].units = Units.GetUnits();
			players[1].units = Units.GetUnits();
			// Set characters
			players[0].characters = new Array(0);
			players[1].characters = new Array(0);
			// Set lastFrame for use in delta time in loop
			lastFrame = getTimer();
			// Start listener for the game loop
			this.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		private function enterFrameHandler(event:Event):void
		{
			Loop();
		}
		
		/*
		 * The main game loop controls timing for the game and what should be done in order
		 */
		private function Loop():void
		{
			// Update delta time
			deltaTime = (getTimer() - lastFrame) / 1000;
			// Only loop if 20ms has passed since last update
			if (deltaTime < 0.02)
			{
				return;
			}
			lastFrame = getTimer();
			// Check if camera wants to be moved
			MoveCamera();
			// Start Attacks
			Attack();
			// Start Movement
			Movement();
			// Render UI
			ui.GameUI();
			// Run AI player
			AI();
			// Check if won or lost
			WinOrLose();
		}
		
		/*
		 *  Sets the difficulty when the button is pressed in UI.as
		 */
		internal function SetDifficulty(difficulty:Number):void
		{
			difficultyModifier = difficulty;
			// Initialize variables and game loop
			InitializeGame();
		}
		
		/*
		 * Creates the sidescrolling effect
		 */ 
		private function MoveCamera():void
		{
			// Left side of screen
			if (stage.mouseX + cameraPosition < cameraPosition + 10)
			{
				cameraPosition = Math.max(-100, Math.min(2672, cameraPosition - (800 * deltaTime)));
			}
			// Right side of screen
			else if (stage.mouseX + cameraPosition > root.width + cameraPosition - 10)
			{
				cameraPosition = Math.max(-100, Math.min(2672, cameraPosition + (800 * deltaTime)));
			}
			// Set new camera position
			cameraView.x = cameraPosition;
			scrollRect = cameraView;
		}
		
		/*
		 * The computer player
		 */ 
		private function AI():void
		{
			// Buy random unit if unit count is less than 5
			if (players[1].characters.length < 5)
			{
				BuyUnit(randomRange(0, 3), 1);
			}
			else
			{
				// Otherwise buy a turret
				BuyTurret(randomRange(3, 5));
			}
			// Research random unit
			ResearchUnit(randomRange(0, 3), 1);
		}
		
		/*
		 * Gets a random number between min and max (both inclusive)
		 */
		private function randomRange(min:Number, max:Number):Number
		{
			var randomNumber:Number = Math.floor(Math.random() * (max - min + 1)) + min;
			return randomNumber;
		}
		
		/*
		 * Manages attacking of characters and bases and turrets
		 */ 
		private function Attack():void
		{
			for (var n:int = 0; n < activeTurrets.length; n++)
			{
				if (n > 2)
				{
					if (players[0].characters.length > 0 && activeTurrets[n])
					{
						if (ui.turretXLocations[n] - players[0].characters[0].characterLocation[0] < 600)
						{
							players[0].characters[0].characterHP -= (n + 1 - 3) * (n + 1 - 3) * 10 * deltaTime;
						}
					}
				}
				else
				{
					if (players[1].characters.length > 0 && activeTurrets[n])
					{
						if (players[1].characters[0].characterLocation[0] - ui.turretXLocations[n] < 600)
						{
							players[1].characters[0].characterHP -= (n + 1) * (n + 1) * 10 * deltaTime;
						}
					}
				}
			}
			// Check if player 1 can attack other characters
			for (var i:int = 0; i < players[0].characters.length; i++)
			{
				var attacked:Boolean = false;
				var characterRange:Number = players[0].characters[i].unit.unitRNG;
				if (players[1].characters.length > 0)
				{
					if (players[1].characters[0].characterLocation[0] - players[0].characters[i].characterLocation[0] < characterRange * 100)
					{
						// Set player animation
						var animationTime:Number = players[0].characters[i].animator.initialTime;
						players[0].characters[i].animator = players[0].characters[i].unit.attackAnimator.Clone();
						players[0].characters[i].animator.initialTime = animationTime;
						attacked = true;
						// In range, check if cooldown has finished
						if ((players[0].characters[i].characterCD - getTimer()) / 1000 <= -players[0].characters[i].unit.unitCD)
						{
							// Reset cooldown
							players[0].characters[i].characterCD = getTimer();
							// Damage opponent
							players[1].characters[0].characterHP -= players[0].characters[i].unit.unitDMG;
						}
					}
				}
				// Check if player 1 can attack the opponents base
				if (Math.abs(players[0].characters[i].characterLocation[0] - players[1].baseLocation[0]) < characterRange * 100)
				{
					// Set player animation
					players[0].characters[i].animator = players[0].characters[i].unit.attackAnimator.Clone();
					attacked = true;
					// Check cooldown
					if ((players[0].characters[i].characterCD - getTimer()) / 1000 <= -players[0].characters[i].unit.unitCD)
					{
						// Reset cooldown
						players[0].characters[i].characterCD = getTimer();
						// Damage opponent
						players[1].baseHP -= players[0].characters[i].unit.unitDMG;
					}
				}
				if (!attacked)
				{
					animationTime = players[0].characters[i].animator.initialTime;
					players[0].characters[i].animator = players[0].characters[i].unit.moveAnimator.Clone();
					players[0].characters[i].animator.initialTime = animationTime;
				}
			}
			// Check if player 2 can attack other characters
			for (i = 0; i < players[1].characters.length; i++)
			{
				attacked = false;
				characterRange = players[1].characters[i].unit.unitRNG;
				if (players[0].characters.length > 0)
				{
					if (players[1].characters[i].characterLocation[0] - players[0].characters[0].characterLocation[0] < characterRange * 100)
					{
						// Set player animation
						animationTime = players[1].characters[i].animator.initialTime;
						players[1].characters[i].animator = players[1].characters[i].unit.attackAnimator.Clone();
						players[1].characters[i].animator.initialTime = animationTime;
						attacked = true;
						// In range, check if cooldown has finished
						if ((players[1].characters[i].characterCD - getTimer()) / 1000 <= -players[1].characters[i].unit.unitCD)
						{
							// Reset cooldown
							players[1].characters[i].characterCD = getTimer();
							// Damage opponent
							players[0].characters[0].characterHP -= players[1].characters[i].unit.unitDMG;
						}
					}
				}
				// Check if player 1 can attack the opponents base
				if (players[1].characters[i].characterLocation[0] - players[0].baseLocation[0] < characterRange * 100)
				{
					// Set player animation
					animationTime = players[1].characters[i].animator.initialTime;
					players[1].characters[i].animator = players[1].characters[i].unit.attackAnimator.Clone();
					players[1].characters[i].animator.initialTime = animationTime;
					attacked = true;
					// Check cooldown
					if ((players[1].characters[i].characterCD - getTimer()) / 1000 <= -players[1].characters[i].unit.unitCD)
					{
						// Reset cooldown
						players[1].characters[i].characterCD = getTimer();
						// Damage opponent
						players[0].baseHP -= players[1].characters[i].unit.unitDMG;
					}
				}
				if (!attacked)
				{
					animationTime = players[1].characters[i].animator.initialTime;
					players[1].characters[i].animator = players[1].characters[i].unit.moveAnimator.Clone();
					players[1].characters[i].animator.initialTime = animationTime;
				}
			}
			
			// Destroy characters that have hp below zero
			for (i = 0; i < players[0].characters.length; i++)
			{
				if (players[0].characters[i].characterHP <= 0)
				{
					// Add gold
					players[0].gold += players[0].characters[i].unit.unitCost * 1.4;
					// Add XP to player that died
					players[1].xp += players[0].characters[i].unit.unitCost * difficultyModifier;
					// Remove character
					players[0].characters.splice(i, 1);
					// Round gold and xp
					players[0].gold = Math.round(players[0].gold);
					players[0].xp = Math.round(players[0].xp);
				}
			}
			for (i = 0; i < players[1].characters.length; i++)
			{
				if (players[1].characters[i].characterHP <= 0)
				{
					// Add gold
					players[1].gold += players[1].characters[i].unit.unitCost * difficultyModifier * 1.4;
					// Add XP to player that died
					players[0].xp += players[1].characters[i].unit.unitCost;
					// Remove character
					players[1].characters.splice(i, 1);
					// Round gold and xp
					players[1].gold = Math.round(players[1].gold);
					players[1].xp = Math.round(players[1].xp);
				}
			}
		}
		
		/*
		 * Manages movement of characters
		 */
		private function Movement():void
		{
			// Player 0 Movement
			for (var i:int = 0; i < players[0].characters.length; i++)
			{
				// Check if character is first in array
				if (i == 0)
				{
					if (players[1].characters.length > 0)
					{
						// Check if character can move closer before collision with enemy
						if (players[1].characters[0].characterLocation[0] - players[0].characters[0].characterLocation[0] >= 90)
						{
							players[0].characters[0].characterLocation[0] += players[0].characters[0].unit.unitSPD * deltaTime * 50;
						}
					}
					else if (players[1].baseLocation[0] - players[0].characters[0].characterLocation[0] > 0)
					{
						players[0].characters[i].characterLocation[0] += players[0].characters[i].unit.unitSPD * deltaTime * 50;
					}
				}
				else
				{
					// Check if character can move closer before collision with one in front
					if (players[0].characters[i - 1].characterLocation[0] - players[0].characters[i].characterLocation[0] >= 85)
					{
						players[0].characters[i].characterLocation[0] += players[0].characters[i].unit.unitSPD * deltaTime * 50;
					}
					else
					{
						players[0].characters[i].characterLocation[0] = players[0].characters[i - 1].characterLocation[0] - 80;
					}
				}
			}
			// Player 1 Movement
			for (i = 0; i < players[1].characters.length; i++)
			{
				// Check if character is first in array
				if (i == 0)
				{
					// Check if character can move closer before collision with enemy
					if (players[0].characters.length > 0)
					{
						if (players[1].characters[0].characterLocation[0] - players[0].characters[0].characterLocation[0] >= 90)
						{
							players[1].characters[0].characterLocation[0] -= players[1].characters[0].unit.unitSPD * deltaTime * 50;
						}
					}
					else if (players[0].baseLocation[0] - players[1].characters[i].characterLocation[0] < 0)
					{
						players[1].characters[i].characterLocation[0] -= players[1].characters[i].unit.unitSPD * deltaTime * 50;
					}
				}
				else
				{
					// Check if character can move closer before collision with one in front
					if (players[1].characters[i].characterLocation[0] - players[1].characters[i - 1].characterLocation[0] >= 85)
					{
						players[1].characters[i].characterLocation[0] -= players[1].characters[i].unit.unitSPD * deltaTime * 50;
					}
					else
					{
						players[1].characters[i].characterLocation[0] = players[1].characters[i - 1].characterLocation[0] + 80;
					}
				}
			}
		}
		
		internal function BuyUnit(entity:int, player:Number):void
		{
			var unitID:int = 0;
			for (var i:int = 0; i < players[player].units.length; i++)
			{
				if (players[player].units[i].unitType == entity && players[player].units[i].isLocked == false)
				{
					unitID = players[player].units[i].unitID;
				}
			}
			if (players[player].gold >= players[player].units[unitID].unitCost)
			{
				// TODO: Buildtime, etc
				players[player].gold -= players[player].units[unitID].unitCost;
				players[player].characters.push(Character.CreateCharacter(players[player], players[player].units, unitID));
			}
		}
		
		internal function ResearchUnit(entity:Number, player:Number):void
		{
			if (players[player].units[entity].isLocked && players[player].xp - players[player].units[entity].unitXPCost >= 0)
			{
				players[player].xp -= players[player].units[entity].unitXPCost;
				players[player].units[entity].isLocked = false;
			}
		}
		
		internal function UpgradeBase(entity:Number):void
		{
			var player:int = 0;
			if (players[player].xp - entity * 20 >= 0)
			{
				players[player].xp -= entity * 20;
				players[player].baseHP *= entity / 12;
				players[player].baseMaxHP *= entity / 12;
			}
		}
		
		/*
		 * Manages the purchasing of turrets
		 */
		internal var turretCosts:Array = new Array(20, 1000, 10000);
		internal var activeTurrets:Array = new Array(false, false, false, false, false, false);
		internal function BuyTurret(turretNumber:Number):void
		{
			if (turretNumber > 2)
			{
				if (players[1].gold >= turretCosts[turretNumber - 3])
				{
					players[1].gold -= turretCosts[turretNumber - 3];
					activeTurrets[turretNumber] = true;
				}
			}
			else
			{
				if (players[0].gold >= turretCosts[turretNumber])
				{
					players[0].gold -= turretCosts[turretNumber];
					activeTurrets[turretNumber] = true;
				}
			}
		}
		
		/*
		 * Check if a player has win or lost
		 */
		internal function WinOrLose():void
		{
			if (players[0].baseHP <= 0)
			{
				ui.YouLoseUI();
			}
			
			if (players[1].baseHP <= 0)
			{
				ui.YouWinUI();
			}
		}
	
	}
}