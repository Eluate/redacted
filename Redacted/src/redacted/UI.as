package redacted
{
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.globalization.CollatorMode;
	import flash.media.SoundChannel;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.display.Bitmap
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	import flash.text.TextFieldAutoSize;
	
	/**
	 * Contains the user interface for the menu and the game
	 * @author Sausage Roll Studios
	 */
	public class UI extends Sprite
	{
		[Embed(source = "ui/Play.png")]
		private static var playImage:Class;
		[Embed(source = "ui/Settings.png")]
		private static var optionsImage:Class;
		[Embed(source = "ui/Help.png")]
		private static var helpImage:Class;
		[Embed(source = "ui/Easy.png")]
		private static var easyImage:Class;
		[Embed(source = "ui/Medium.png")]
		private static var normalImage:Class;
		[Embed(source = "ui/Hard.png")]
		private static var hardImage:Class;
		[Embed(source = "ui/Dog.png")]
		private static var insaneImage:Class;
		[Embed(source = "ui/Back.png")]
		private static var backImage:Class;
		[Embed(source = "ui/Melee.png")]
		private static var meleeImage:Class;
		[Embed(source = "ui/Research.png")]
		private static var researchImage:Class;
		[Embed(source = "ui/Ranged.png")]
		private static var rangeImage:Class;
		[Embed(source = "ui/Tanksmall.png")]
		private static var tankImage:Class;
		[Embed(source = "ui/Ability.png")]
		private static var specialImage:Class;
		[Embed(source = "ui/Turret1.png")]
		private static var turret1Image:Class;
		[Embed(source = "ui/Turret2.png")]
		private static var turret2Image:Class;
		[Embed(source = "ui/Turret3.png")]
		private static var turret3Image:Class;
		[Embed(source = "ui/Special.png")]
		private static var indiImage:Class;
		[Embed(source = "ui/test.mp3")]
		private static var music1:Class;
		[Embed(source = "ui/Music.png")]
		private static var muteImage:Class;
		[Embed(source = "ui/Background.jpg")]
		private static var backgroundImage:Class;
		[Embed(source = "ui/Gold.png")]
		private static var goldImage:Class;
		[Embed(source = "ui/XP.png")]
		private static var xpImage:Class;
		[Embed(source = "ui/HealthBG.png")]
		private static var healthBG:Class;
		[Embed(source = "ui/HealthFill.png")]
		private static var healthFill:Class;
		[Embed(source = "ui/Castle.png")]
		private static var baseImage:Class;
		[Embed(source = "ui/Lose.png")]
		private static var loseImage:Class;
		[Embed(source = "ui/Win.png")]
		private static var winImage:Class;
		[Embed(source = "ui/Rpanel.jpg")]
		private static var panelImage:Class;
		[Embed(source = "ui/Pank.png")]
		private static var pankImage:Class;
		[Embed(source = "ui/Pellee.png")]
		private static var pelleeImage:Class;
		[Embed(source = "ui/Panged.png")]
		private static var pangedImage:Class;
		[Embed(source = "ui/Ppecial.png")]
		private static var pecialImage:Class;
		[Embed(source = "ui/castlebutton.jpg")]
		private static var pastleImage:Class;
		
		// Instance of game
		internal var game:Game;
		// Function variables
		private var researchToggle:Boolean;
		private var sound:Sound;
		private var myChannel:SoundChannel = new SoundChannel();
		private var volume:int = 0.5;
		
		public function MenuUI(event:MouseEvent):void
		{
			//Background
			var backgroundBmp:Bitmap = new backgroundImage();
			var backgroundMatrix:Matrix = new Matrix();
			var backgroundSprite:Sprite = new Sprite();
			backgroundMatrix.translate(-200, 0);
			backgroundSprite.graphics.beginBitmapFill(backgroundBmp.bitmapData, backgroundMatrix, true);
			backgroundSprite.graphics.drawRect(-200, 0, backgroundBmp.width, backgroundBmp.height);
			addChild(backgroundSprite);
			
			sound = (new music1) as Sound;
			myChannel.stop();
			myChannel = sound.play(0, 999);
			myChannel.soundTransform = new SoundTransform(volume);
			
			// Play Button
			var playButton:SimpleButton = new SimpleButton();
			var playBmp:Bitmap = new playImage();
			var playMatrix:Matrix = new Matrix();
			var playSprite:Sprite = new Sprite();
			playMatrix.translate(412, 200);
			playSprite.graphics.beginBitmapFill(playBmp.bitmapData, playMatrix, true);
			playSprite.graphics.drawRect(412, 200, playBmp.width, playBmp.height);
			playButton.overState = playButton.downState = playButton.upState = playButton.hitTestState = playSprite;
			playButton.addEventListener(MouseEvent.CLICK, DifficultyUI)
			addChild(playButton);
			
			// Options Button
			var optionsButton:SimpleButton = new SimpleButton();
			var optionsBmp:Bitmap = new optionsImage();
			var optionsMatrix:Matrix = new Matrix();
			var optionsSprite:Sprite = new Sprite();
			optionsMatrix.translate(412, 400);
			optionsSprite.graphics.beginBitmapFill(optionsBmp.bitmapData, optionsMatrix, true);
			optionsSprite.graphics.drawRect(412, 400, optionsBmp.width, optionsBmp.height);
			optionsButton.overState = optionsButton.downState = optionsButton.upState = optionsButton.hitTestState = optionsSprite;
			optionsButton.addEventListener(MouseEvent.CLICK, OptionsUI)
			addChild(optionsButton);
			
			// Help Button
			var helpButton:SimpleButton = new SimpleButton();
			var helpBmp:Bitmap = new helpImage();
			var helpMatrix:Matrix = new Matrix();
			var helpSprite:Sprite = new Sprite();
			helpMatrix.translate(412, 550);
			helpSprite.graphics.beginBitmapFill(helpBmp.bitmapData, helpMatrix, true);
			helpSprite.graphics.drawRect(412, 550, helpBmp.width, helpBmp.height);
			helpButton.overState = helpButton.downState = helpButton.upState = helpButton.hitTestState = helpSprite;
			helpButton.addEventListener(MouseEvent.CLICK, HelpUI)
			addChild(helpButton);
		}
		
		public function OptionsUI(event:MouseEvent):void
		{
			//Background
			var backgroundBmp:Bitmap = new backgroundImage();
			var backgroundMatrix:Matrix = new Matrix();
			var backgroundSprite:Sprite = new Sprite();
			backgroundMatrix.translate(-200, 0);
			backgroundSprite.graphics.beginBitmapFill(backgroundBmp.bitmapData, backgroundMatrix, true);
			backgroundSprite.graphics.drawRect(-200, 0, backgroundBmp.width, backgroundBmp.height);
			addChild(backgroundSprite);
			
			//Back Button
			var backButton:SimpleButton = new SimpleButton();
			var backBmp:Bitmap = new backImage();
			var backMatrix:Matrix = new Matrix();
			var backSprite:Sprite = new Sprite();
			backMatrix.translate(0, 100);
			backSprite.graphics.beginBitmapFill(backBmp.bitmapData, backMatrix, true);
			backSprite.graphics.drawRect(750, 600, backBmp.width, backBmp.height);
			backButton.overState = backButton.downState = backButton.upState = backButton.hitTestState = backSprite;
			backButton.addEventListener(MouseEvent.CLICK, MenuUI);
			addChild(backButton);
			
			//Mute Button
			var muteButton:SimpleButton = new SimpleButton();
			var muteBmp:Bitmap = new muteImage();
			var muteMatrix:Matrix = new Matrix();
			var muteSprite:Sprite = new Sprite();
			muteMatrix.translate(0, 0);
			muteSprite.graphics.beginBitmapFill(muteBmp.bitmapData, muteMatrix, true);
			muteSprite.graphics.drawRect(0, 0, muteBmp.width, muteBmp.height);
			muteButton.overState = muteButton.downState = muteButton.upState = muteButton.hitTestState = muteSprite;
			muteButton.addEventListener(MouseEvent.CLICK, onClickStop);
			addChild(muteButton);
		}
		
		private function onClickStop(e:MouseEvent):void
		{
			if (volume == 0)
			{
				volume = 1;
			}
			else
			{
				volume = 0;
			}
			myChannel.soundTransform = new SoundTransform(volume);
		}
		
		public function DifficultyUI(event:MouseEvent):void
		{
			//Background
			var backgroundBmp:Bitmap = new backgroundImage();
			var backgroundMatrix:Matrix = new Matrix();
			var backgroundSprite:Sprite = new Sprite();
			backgroundMatrix.translate(-200, 0);
			backgroundSprite.graphics.beginBitmapFill(backgroundBmp.bitmapData, backgroundMatrix, true);
			backgroundSprite.graphics.drawRect(-200, 0, backgroundBmp.width, backgroundBmp.height);
			addChild(backgroundSprite);
			
			//Easy Diff Button
			var easyButton:SimpleButton = new SimpleButton();
			var easyBmp:Bitmap = new easyImage();
			var easyMatrix:Matrix = new Matrix();
			var easySprite:Sprite = new Sprite();
			easyMatrix.translate(412, 90);
			easySprite.graphics.beginBitmapFill(easyBmp.bitmapData, easyMatrix, true);
			easySprite.graphics.drawRect(412, 90, easyBmp.width, easyBmp.height);
			easyButton.overState = easyButton.downState = easyButton.upState = easyButton.hitTestState = easySprite;
			easyButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
			{
				game.SetDifficulty(1)
			});
			addChild(easyButton);
			
			//Normal Diff Button
			var normalButton:SimpleButton = new SimpleButton();
			var normalBmp:Bitmap = new normalImage();
			var normalMatrix:Matrix = new Matrix();
			var normalSprite:Sprite = new Sprite();
			normalMatrix.translate(412, 180);
			normalSprite.graphics.beginBitmapFill(normalBmp.bitmapData, normalMatrix, true);
			normalSprite.graphics.drawRect(412, 180, normalBmp.width, normalBmp.height);
			normalButton.overState = normalButton.downState = normalButton.upState = normalButton.hitTestState = normalSprite;
			normalButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
			{
				game.SetDifficulty(1.2)
			})
			addChild(normalButton);
			
			//Hard Diff Button
			var hardButton:SimpleButton = new SimpleButton();
			var hardBmp:Bitmap = new hardImage();
			var hardMatrix:Matrix = new Matrix();
			var hardSprite:Sprite = new Sprite();
			hardMatrix.translate(412, 270);
			hardSprite.graphics.beginBitmapFill(hardBmp.bitmapData, hardMatrix, true);
			hardSprite.graphics.drawRect(412, 270, hardBmp.width, hardBmp.height);
			hardButton.overState = hardButton.downState = hardButton.upState = hardButton.hitTestState = hardSprite;
			hardButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
			{
				game.SetDifficulty(1.5)
			})
			addChild(hardButton);
			
			//Insane Diff Button
			var insaneButton:SimpleButton = new SimpleButton();
			var insaneBmp:Bitmap = new insaneImage();
			var insaneMatrix:Matrix = new Matrix();
			var insaneSprite:Sprite = new Sprite();
			insaneMatrix.translate(412, 360);
			insaneSprite.graphics.beginBitmapFill(insaneBmp.bitmapData, insaneMatrix, true);
			insaneSprite.graphics.drawRect(412, 360, insaneBmp.width, insaneBmp.height);
			insaneButton.overState = insaneButton.downState = insaneButton.upState = insaneButton.hitTestState = insaneSprite;
			insaneButton.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):void
			{
				game.SetDifficulty(2)
			})
			addChild(insaneButton);
			
			//Back Button
			var backButton:SimpleButton = new SimpleButton();
			var backBmp:Bitmap = new backImage();
			var backMatrix:Matrix = new Matrix();
			var backSprite:Sprite = new Sprite();
			backMatrix.translate(750, 600);
			backSprite.graphics.beginBitmapFill(backBmp.bitmapData, backMatrix, true);
			backSprite.graphics.drawRect(750, 600, backBmp.width, backBmp.height);
			backButton.overState = backButton.downState = backButton.upState = backButton.hitTestState = backSprite;
			backButton.addEventListener(MouseEvent.CLICK, MenuUI);
			addChild(backButton);
		}
		
		public function HelpUI(event:MouseEvent):void
		{
			//Background
			var backgroundBmp:Bitmap = new backgroundImage();
			var backgroundMatrix:Matrix = new Matrix();
			var backgroundSprite:Sprite = new Sprite();
			backgroundMatrix.translate(-200, 0);
			backgroundSprite.graphics.beginBitmapFill(backgroundBmp.bitmapData, backgroundMatrix, true);
			backgroundSprite.graphics.drawRect(-200, 0, backgroundBmp.width, backgroundBmp.height);
			addChild(backgroundSprite);
			
			//Back Button
			var backButton:SimpleButton = new SimpleButton();
			var backBmp:Bitmap = new backImage();
			var backMatrix:Matrix = new Matrix();
			var backSprite:Sprite = new Sprite();
			backMatrix.translate(600, 500);
			backSprite.graphics.beginBitmapFill(backBmp.bitmapData, backMatrix, true);
			backSprite.graphics.drawRect(750, 600, backBmp.width, backBmp.height);
			backButton.overState = backButton.downState = backButton.upState = backButton.hitTestState = backSprite;
			backButton.addEventListener(MouseEvent.CLICK, MenuUI);
			addChild(backButton);
			
			//Text
			var myFormat:TextFormat = new TextFormat();
			myFormat.size = 40;
			var myText:TextField = new TextField();
			myText.defaultTextFormat = myFormat;
			myText.text = "Hello World";
			addChild(myText);
			myText.text = "Welcome to Redacted, this is a 2D strategy-type game. There are two bases on each side, and the aim of the game is to destroy the opponent's base on the other side of the map. You do this by sending units forward, while defending your base from incoming enemies from the opposite base. It is recommended that you play on the 'Easy' difficulty setting if you are a newcomer and want to learn what different buttons do. The main controls are using your mouse to press buttons. Simple! This game, Redacted was created by Jack, Harold and Chris.";
			myText.textColor = 0xFF0000;
			myText.border = true;
			myText.borderColor = 0xFF0000;
			myText.background = true;
			myText.backgroundColor = 0x000000;
			myText.wordWrap = true;
			myText.width = 600;
			myText.height = 600;
			myText.x = 200;
			myText.y = 0;
		}
		
		public function SizeCheck(image:Rectangle):Boolean
		{
			if (image.width + image.x < game.cameraPosition)
			{
				return false;
			}
			if (image.x - (image.width / 2) > game.cameraPosition + 1024)
			{
				return false;
			}
			if (image.height + image.y < 0)
			{
				return false;
			}
			if (image.y - (image.height / 2) > 1024)
			{
				return false;
			}
			return true;
		}
		
		public function GameUI():void
		{
			// Remove all children sprites
			while (numChildren > 0)
			{
				removeChildAt(0);
			}
			
			//Background
			var backgroundBmp:Bitmap = new backgroundImage();
			var backgroundMatrix:Matrix = new Matrix();
			var backgroundSprite:Sprite = new Sprite();
			backgroundMatrix.translate(-200, 0);
			backgroundSprite.graphics.beginBitmapFill(backgroundBmp.bitmapData, backgroundMatrix, true);
			backgroundSprite.graphics.drawRect(-200, 0, backgroundBmp.width, backgroundBmp.height);
			addChild(backgroundSprite);
			
			//Left Base
			var baseBmp:Bitmap = new baseImage();
			var baseMatrix:Matrix = new Matrix();
			var baseSprite:Sprite = new Sprite();
			baseMatrix.translate(-200, 49);
			baseSprite.graphics.beginBitmapFill(baseBmp.bitmapData, baseMatrix, true);
			baseSprite.graphics.drawRect(-200, 50, baseBmp.width, baseBmp.height);
			addChild(baseSprite);
			
			// LB Render health bar background
			var leftHealthBmp:Bitmap = new healthBG();
			var leftHealthMatrix:Matrix = new Matrix();
			var leftHealthSprite:Sprite = new Sprite();
			leftHealthMatrix.translate(-10, 660);
			leftHealthSprite.graphics.beginBitmapFill(leftHealthBmp.bitmapData, leftHealthMatrix, true);
			leftHealthSprite.graphics.drawRect(-10, 660, leftHealthBmp.width, leftHealthBmp.height);
			leftHealthSprite.scaleX = 5;
			addChild(leftHealthSprite);
			
			// LB Render health fill
			var LefthBmp:Bitmap = new healthFill();
			var LefthMatrix:Matrix = new Matrix();
			var LefthSprite:Sprite = new Sprite();
			LefthBmp.width = game.players[0].baseHP / game.players[0].baseMaxHP * 68;
			LefthSprite.scaleX = 5;
			LefthSprite.graphics.beginBitmapFill(LefthBmp.bitmapData, LefthMatrix, true);
			LefthSprite.graphics.drawRect(-9, 660 + 1, LefthBmp.width, LefthBmp.height);
			addChild(LefthSprite);
			
			//Right Base
			var base2Bmp:Bitmap = new baseImage();
			var base2Matrix:Matrix = new Matrix();
			var base2Sprite:Sprite = new Sprite();
			base2Matrix.translate(3400, 50);
			base2Matrix.scale(-1, 1);
			base2Sprite.graphics.beginBitmapFill(base2Bmp.bitmapData, base2Matrix, true);
			base2Sprite.graphics.drawRect(3200, 50, base2Bmp.width, base2Bmp.height);
			addChild(base2Sprite);
			
			// RB Render health bar background
			var RightHealthBmp:Bitmap = new healthBG();
			var RightHealthMatrix:Matrix = new Matrix();
			var RightHealthSprite:Sprite = new Sprite();
			RightHealthMatrix.translate(660, 660);
			RightHealthSprite.graphics.beginBitmapFill(RightHealthBmp.bitmapData, RightHealthMatrix, true);
			RightHealthSprite.graphics.drawRect(660, 660, RightHealthBmp.width, RightHealthBmp.height);
			RightHealthSprite.scaleX = 5;
			addChild(RightHealthSprite);
			
			// RB Render health fill
			var RighthBmp:Bitmap = new healthFill();
			var RighthMatrix:Matrix = new Matrix();
			var RighthSprite:Sprite = new Sprite();
			RighthBmp.width = game.players[1].baseHP / game.players[1].baseMaxHP * 68;
			RighthSprite.scaleX = 5;
			RighthSprite.graphics.beginBitmapFill(RighthBmp.bitmapData, RighthMatrix, true);
			RighthSprite.graphics.drawRect(660, 660 + 1, RighthBmp.width, RighthBmp.height);
			addChild(RighthSprite);
			
			// XP Text
			var xpFormat:TextFormat = new TextFormat();
			xpFormat.size = 40;
			xpFormat.align = "right";
			var xpText:TextField = new TextField();
			xpText.defaultTextFormat = xpFormat;
			xpText.selectable = false;
			xpText.text = game.players[0].xp;
			xpText.textColor = 0xCA35B0;
			xpText.wordWrap = true;
			xpText.width = 120;
			xpText.height = 150;
			xpText.x = 0 + game.cameraPosition;
			xpText.y = 660;
			addChild(xpText);
			
			//Gold text
			var goldFormat:TextFormat = new TextFormat();
			goldFormat.size = 40;
			goldFormat.align = "right";
			var goldText:TextField = new TextField();
			goldText.defaultTextFormat = goldFormat;
			goldText.selectable = false;
			goldText.text = game.players[0].gold;
			goldText.textColor = 0xFFFF00;
			goldText.wordWrap = true;
			goldText.width = 120;
			goldText.height = 150;
			goldText.x = 170 + game.cameraPosition;
			goldText.y = 660;
			addChild(goldText);
			
			// Render characters
			for (var n:int = 0; n < game.players.length; n++)
			{
				for (var i:int = 0; i < game.players[n].characters.length; i++)
				{
					var character:Object = game.players[n].characters[i];
					var characterBmp:Bitmap = character.animator.GetAnimationImage();
					if (SizeCheck(new Rectangle(character.characterLocation[0], 500, characterBmp.width, characterBmp.height)))
					{
						// Render the sprite
						var characterMatrix:Matrix = new Matrix();
						var characterSprite:Sprite = new Sprite();
						if (n == 1) {
							characterMatrix.scale( -1, 1);
						}
						characterMatrix.translate(character.characterLocation[0], 470);
						characterSprite.graphics.beginBitmapFill(characterBmp.bitmapData, characterMatrix, true);
						characterSprite.graphics.drawRect(character.characterLocation[0], 470, characterBmp.width, characterBmp.height);
						addChild(characterSprite);
						
						// Render health bar background
						var healthBmp:Bitmap = new healthBG();
						var healthMatrix:Matrix = new Matrix();
						var healthSprite:Sprite = new Sprite();
						healthMatrix.translate(character.characterLocation[0], 550 + (characterBmp.height / 2));
						healthSprite.graphics.beginBitmapFill(healthBmp.bitmapData, healthMatrix, true);
						healthSprite.graphics.drawRect(character.characterLocation[0], 550 + (characterBmp.height / 2), healthBmp.width, healthBmp.height);
						addChild(healthSprite);
						
						// Render health fill
						var hBmp:Bitmap = new healthFill();
						var hMatrix:Matrix = new Matrix();
						var hSprite:Sprite = new Sprite();
						hBmp.width = character.characterHP / character.unit.unitMaxHP * 68;
						healthMatrix.translate(character.characterLocation[0], 550 + (characterBmp.height / 2) + 50);
						hSprite.graphics.beginBitmapFill(hBmp.bitmapData, hMatrix, true);
						hSprite.graphics.drawRect(character.characterLocation[0] + 1, 551 + (characterBmp.height / 2), hBmp.width, hBmp.height);
						addChild(hSprite);
					}
				}
			}
			
			//Xp Symbol
			var xpBmp:Bitmap = new xpImage();
			var xpMatrix:Matrix = new Matrix();
			var xpSprite:Sprite = new Sprite();
			xpMatrix.translate(120 + game.cameraPosition, 670);
			xpSprite.graphics.beginBitmapFill(xpBmp.bitmapData, xpMatrix, true);
			xpSprite.graphics.drawRect(120 + game.cameraPosition, 670, xpBmp.width, xpBmp.height);
			addChild(xpSprite);
			
			//Gold Symbol 
			var goldBmp:Bitmap = new goldImage();
			var goldMatrix:Matrix = new Matrix();
			var goldSprite:Sprite = new Sprite();
			goldMatrix.translate(290 + game.cameraPosition, 670);
			goldSprite.graphics.beginBitmapFill(goldBmp.bitmapData, goldMatrix, true);
			goldSprite.graphics.drawRect(290 + game.cameraPosition, 670, goldBmp.width, goldBmp.height);
			addChild(goldSprite);
			
			//Spawn Melee Character
			var meleeButton:SimpleButton = new SimpleButton();
			var meleeBmp:Bitmap = new meleeImage();
			var meleeMatrix:Matrix = new Matrix();
			var meleeSprite:Sprite = new Sprite();
			meleeMatrix.translate(game.cameraPosition + 0, 0);
			meleeSprite.graphics.beginBitmapFill(meleeBmp.bitmapData, meleeMatrix, true);
			meleeSprite.graphics.drawRect(game.cameraPosition, 0, 50, 50);
			meleeButton.overState = meleeButton.downState = meleeButton.upState = meleeButton.hitTestState = meleeSprite;
			meleeButton.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
			{
				game.BuyUnit(0, 0)
			});
			meleeButton.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
			{
				ShowGoldCost(0)
			});
			addChild(meleeButton);
			
			//Spawn Tank Character
			var tankButton:SimpleButton = new SimpleButton();
			var tankBmp:Bitmap = new tankImage();
			var tankMatrix:Matrix = new Matrix();
			var tankSprite:Sprite = new Sprite();
			tankMatrix.translate(game.cameraPosition + 0, 100);
			tankSprite.graphics.beginBitmapFill(tankBmp.bitmapData, tankMatrix, true);
			tankSprite.graphics.drawRect(game.cameraPosition, 100, 50, 50);
			tankButton.overState = tankButton.downState = tankButton.upState = tankButton.hitTestState = tankSprite;
			tankButton.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
			{
				game.BuyUnit(1, 0)
			});
			tankButton.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
			{
				ShowGoldCost(1)
			});
			addChild(tankButton);
			
			//Spawn Range Character
			var rangeButton:SimpleButton = new SimpleButton();
			var rangeBmp:Bitmap = new rangeImage();
			var rangeMatrix:Matrix = new Matrix();
			var rangeSprite:Sprite = new Sprite();
			rangeMatrix.translate(game.cameraPosition + 0, 200);
			rangeSprite.graphics.beginBitmapFill(rangeBmp.bitmapData, rangeMatrix, true);
			rangeSprite.graphics.drawRect(game.cameraPosition, 200, 50, 50);
			rangeButton.overState = rangeButton.downState = rangeButton.upState = rangeButton.hitTestState = rangeSprite;
			rangeButton.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
			{
				game.BuyUnit(2, 0)
			});
			rangeButton.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
			{
				ShowGoldCost(2)
			});
			addChild(rangeButton);
			
			//Spawn Special Character
			var indiButton:SimpleButton = new SimpleButton();
			var indiBmp:Bitmap = new indiImage();
			var indiMatrix:Matrix = new Matrix();
			var indiSprite:Sprite = new Sprite();
			indiMatrix.translate(game.cameraPosition + 0, 300);
			indiSprite.graphics.beginBitmapFill(indiBmp.bitmapData, indiMatrix, true);
			indiSprite.graphics.drawRect(game.cameraPosition, 300, indiBmp.width, indiBmp.height);
			indiButton.overState = indiButton.downState = indiButton.upState = indiButton.hitTestState = indiSprite;
			indiButton.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
			{
				game.BuyUnit(3, 0)
			});
			indiButton.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
			{
				ShowGoldCost(3)
			});
			addChild(indiButton);
			
			//Turret Button
			if (!game.activeTurrets[0])
			{
				var turretButton:SimpleButton = new SimpleButton();
				var turretBmp:Bitmap = new turret1Image();
				var turretMatrix:Matrix = new Matrix();
				var turretSprite:Sprite = new Sprite();
				turretMatrix.translate(game.cameraPosition + 200, 0);
				turretSprite.graphics.beginBitmapFill(turretBmp.bitmapData, turretMatrix, true);
				turretSprite.graphics.drawRect(game.cameraPosition + 200, 0, turretBmp.width, turretBmp.height);
				turretButton.overState = turretButton.downState = turretButton.upState = turretButton.hitTestState = turretSprite;
				turretButton.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
				{
					game.BuyTurret(0)
				});
				turretButton.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
				{
					ShowTowerHover(0)
				});
				addChild(turretButton);
			}
			
			//Turret 2 Button
			if (!game.activeTurrets[1])
			{
				var turret2Button:SimpleButton = new SimpleButton();
				var turret2Bmp:Bitmap = new turret2Image();
				var turret2Matrix:Matrix = new Matrix();
				var turret2Sprite:Sprite = new Sprite();
				turret2Matrix.translate(game.cameraPosition + 200, 100);
				turret2Sprite.graphics.beginBitmapFill(turret2Bmp.bitmapData, turret2Matrix, true);
				turret2Sprite.graphics.drawRect(game.cameraPosition + 200, 100, turret2Bmp.width, turret2Bmp.height);
				turret2Button.overState = turret2Button.downState = turret2Button.upState = turret2Button.hitTestState = turret2Sprite;
				turret2Button.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
				{
					game.BuyTurret(1)
				});
				turret2Button.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
				{
					ShowTowerHover(1)
				});
				addChild(turret2Button);
			}
			
			//Turret 3 Button
			if (!game.activeTurrets[2])
			{
				var turret3Button:SimpleButton = new SimpleButton();
				var turret3Bmp:Bitmap = new turret3Image();
				var turret3Matrix:Matrix = new Matrix();
				var turret3Sprite:Sprite = new Sprite();
				turret3Matrix.translate(game.cameraPosition + 200, 200);
				turret3Sprite.graphics.beginBitmapFill(turret3Bmp.bitmapData, turret3Matrix, true);
				turret3Sprite.graphics.drawRect(game.cameraPosition + 200, 200, turret2Bmp.width, turret2Bmp.height);
				turret3Button.overState = turret3Button.downState = turret3Button.upState = turret3Button.hitTestState = turret3Sprite;
				turret3Button.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
				{
					game.BuyTurret(2)
				});
				turret3Button.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
				{
					ShowTowerHover(2)
				});
				addChild(turret3Button);
			}
			
			RenderTurrets();
			
			if (researchToggle)
			{
				//Research UI Panel
				var panelBmp:Bitmap = new panelImage();
				var panelMatrix:Matrix = new Matrix();
				var panelSprite:Sprite = new Sprite();
				panelMatrix.translate(game.cameraPosition + 50, 50);
				panelSprite.graphics.beginBitmapFill(panelBmp.bitmapData, panelMatrix, true);
				panelSprite.graphics.drawRect(game.cameraPosition + 50, 50, panelBmp.width, panelBmp.height);
				addChild(panelSprite);
				
				var locationsX:Array = new Array(16, 16, 16, 16, 16, 138, 138, 138, 138, 138, 255, 255, 255, 255, 255, 370, 370, 370, 370, 370, 485, 485, 485, 485, 485);
				var locationsY:Array = new Array(16, 138, 260, 385, 505, 16, 138, 260, 385, 505, 16, 138, 260, 385, 505, 16, 138, 260, 385, 505, 16, 138, 260, 385, 505);
				var unitEntities:Array = new Array(0, 4, 8, 12, 16, 1, 5, 9, 13, 17, 2, 6, 10, 14, 18, 3, 7, 11, 15, 19, 20, 25, 30, 35, 40);
				var unitImages:Array = new Array(pelleeImage, pelleeImage, pelleeImage, pelleeImage, pelleeImage, pangedImage, pangedImage, pangedImage, pangedImage, pangedImage, pankImage, pankImage, pankImage, pankImage, pankImage, pecialImage, pecialImage, pecialImage, pecialImage, pecialImage, pastleImage, pastleImage, pastleImage, pastleImage, pastleImage);
				
				// Render image fills for research panel
				for (i = 0; i < locationsX.length; i++)
				{
					ResearchIndividualButtonUI(locationsX[i], locationsY[i], unitEntities[i], unitImages[i]);
				}
			}
			
			//Research Button
			var researchButton:SimpleButton = new SimpleButton();
			var researchBmp:Bitmap = new researchImage();
			var researchMatrix:Matrix = new Matrix();
			var researchSprite:Sprite = new Sprite();
			researchMatrix.translate(game.cameraPosition + 750, 0);
			researchSprite.graphics.beginBitmapFill(researchBmp.bitmapData, researchMatrix, true);
			researchSprite.graphics.drawRect(game.cameraPosition + 750, 0, researchBmp.width, researchBmp.height);
			researchButton.overState = researchButton.downState = researchButton.upState = researchButton.hitTestState = researchSprite;
			researchButton.addEventListener(MouseEvent.MOUSE_DOWN, ResearchUI);
			addChild(researchButton);
		}
		
		private var turretImages:Array = new Array(turret1Image, turret2Image, turret3Image);
		internal var turretXLocations:Array = new Array(100, 100, 100, 3100, 3100, 3100);
		private var turretYLocations:Array = new Array(80, 250, 420);
		
		private function RenderTurrets():void
		{
			for (var i:int = 0; i < game.activeTurrets.length; i++)
			{
				if (game.activeTurrets[i] == true)
				{
					if (i < 3) {
						// Render turret
						var turretBmp:Bitmap = new turretImages[i]();
						var turretMatrix:Matrix = new Matrix();
						var turretSprite:Sprite = new Sprite();
						turretMatrix.translate(turretXLocations[i], turretYLocations[i]);
						turretSprite.graphics.beginBitmapFill(turretBmp.bitmapData, turretMatrix, true);
						turretSprite.graphics.drawRect(turretXLocations[i], turretYLocations[i], turretBmp.width, turretBmp.height);
						addChild(turretSprite);
					} else {
						// Render turret
						var turretBmp:Bitmap = new turretImages[i - 3]();
						var turretMatrix:Matrix = new Matrix();
						var turretSprite:Sprite = new Sprite();
						turretMatrix.translate(turretXLocations[i], turretYLocations[i - 3]);
						turretSprite.graphics.beginBitmapFill(turretBmp.bitmapData, turretMatrix, true);
						turretSprite.graphics.drawRect(turretXLocations[i], turretYLocations[i - 3], turretBmp.width, turretBmp.height);
						addChild(turretSprite);
					}
				}
			}
		}
		
		private function ShowGoldCost(entity:int):void
		{
			var unitID:int = 0;
			for (var i:int = 0; i < game.players[0].units.length; i++)
			{
				if (game.players[0].units[i].unitType == entity && game.players[0].units[i].isLocked == false)
				{
					unitID = game.players[0].units[i].unitID;
				}
			}
			
			// Display Text
			var goldFormat:TextFormat = new TextFormat();
			goldFormat.size = 24;
			goldFormat.align = "center";
			var goldText:TextField = new TextField();
			goldText.defaultTextFormat = goldFormat;
			goldText.selectable = false;
			goldText.text = game.players[0].units[unitID].unitName + "\n" + game.players[0].units[unitID].unitCost;
			goldText.textColor = 0xFFFF00;
			goldText.autoSize = TextFieldAutoSize.LEFT;
			goldText.multiline = true;
			goldText.wordWrap = true;
			goldText.border = true;
			goldText.borderColor = 0xF7AF09;
			goldText.background = true;
			goldText.backgroundColor = 0x000000
			goldText.x = mouseX + 5;
			goldText.y = mouseY + 5;
			addChild(goldText);
		}
		
		private function ShowTowerHover(entity:Number):void
		{
			// Display Text
			var goldFormat:TextFormat = new TextFormat();
			goldFormat.size = 24;
			goldFormat.align = "center";
			var goldText:TextField = new TextField();
			goldText.defaultTextFormat = goldFormat;
			goldText.selectable = false;
			goldText.text = "Turret: " + (entity + 1) + "\n" + game.turretCosts[entity];
			goldText.textColor = 0xFFFF00;
			goldText.autoSize = TextFieldAutoSize.LEFT;
			goldText.multiline = true;
			goldText.wordWrap = true;
			goldText.border = true;
			goldText.borderColor = 0xF7AF09;
			goldText.background = true;
			goldText.backgroundColor = 0x000000
			goldText.x = mouseX + 5;
			goldText.y = mouseY + 5;
			addChild(goldText);
		}
		
		private function ShowResearchHover(entity:Number):void
		{
			// Display Text
			var goldFormat:TextFormat = new TextFormat();
			goldFormat.size = 24;
			goldFormat.align = "center";
			var goldText:TextField = new TextField();
			goldText.defaultTextFormat = goldFormat;
			goldText.selectable = false;
			if (game.players[0].units.length > entity)
			{
				goldText.text = game.players[0].units[entity].unitName + "\n" + game.players[0].units[entity].unitXPCost + "\n" + "L: " + game.players[0].units[entity].isLocked;
			}
			goldText.textColor = 0xFFFF00;
			goldText.autoSize = TextFieldAutoSize.LEFT;
			goldText.multiline = true;
			goldText.wordWrap = true;
			goldText.border = true;
			goldText.borderColor = 0xF7AF09;
			goldText.background = true;
			goldText.backgroundColor = 0x000000
			goldText.x = mouseX + 5;
			goldText.y = mouseY + 5;
			addChild(goldText);
		}
		
		private function ShowCastleResearchHover(entity:Number):void
		{
			// Display Text
			var goldFormat:TextFormat = new TextFormat();
			goldFormat.size = 24;
			goldFormat.align = "center";
			var goldText:TextField = new TextField();
			goldText.defaultTextFormat = goldFormat;
			goldText.selectable = false;
			goldText.text = "HP*" + (entity / 14).toFixed(1) + "\n" + (entity * 20);
			goldText.textColor = 0xFFFF00;
			goldText.autoSize = TextFieldAutoSize.LEFT;
			goldText.multiline = true;
			goldText.wordWrap = true;
			goldText.border = true;
			goldText.borderColor = 0xF7AF09;
			goldText.background = true;
			goldText.backgroundColor = 0x000000
			goldText.x = mouseX + 5;
			goldText.y = mouseY + 5;
			addChild(goldText);
		}
		
		private function ResearchIndividualButtonUI(xLocation:Number, yLocation:Number, entity:Number, image:Class):void
		{
			// Render button
			if (game.players[0].units.length > entity) {
				var individualButton:SimpleButton = new SimpleButton();
				var individualBmp:Bitmap = new image();
				var individualMatrix:Matrix = new Matrix();
				var individualSprite:Sprite = new Sprite();
				individualMatrix.translate(xLocation + 51 + game.cameraPosition, yLocation + 51);
				individualSprite.graphics.beginBitmapFill(individualBmp.bitmapData, individualMatrix, true);
				individualSprite.graphics.drawRect(xLocation + 51 + game.cameraPosition, yLocation + 51, individualBmp.width, individualBmp.height);
				individualButton.overState = individualButton.downState = individualButton.upState = individualButton.hitTestState = individualSprite;
				individualButton.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
				{
					game.ResearchUnit(entity, 0);
				});
				individualButton.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
				{
					ShowResearchHover(entity);
				});
				addChild(individualButton);
			} else {
				// Castle upgrade
				individualButton = new SimpleButton();
				individualBmp = new image();
				individualMatrix = new Matrix();
				individualSprite = new Sprite();
				individualMatrix.translate(xLocation + 51 + game.cameraPosition, yLocation + 51);
				individualSprite.graphics.beginBitmapFill(individualBmp.bitmapData, individualMatrix, true);
				individualSprite.graphics.drawRect(xLocation + 51 + game.cameraPosition, yLocation + 51, individualBmp.width, individualBmp.height);
				individualButton.overState = individualButton.downState = individualButton.upState = individualButton.hitTestState = individualSprite;
				individualButton.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void
				{
					game.UpgradeBase(entity);
				});
				individualButton.addEventListener(MouseEvent.MOUSE_OVER, function(e:MouseEvent):void
				{
					ShowCastleResearchHover(entity);
				});
				addChild(individualButton);
			}
		}
		
		internal function YouLoseUI():void
		{
			var loseBmp:Bitmap = new loseImage();
			var loseMatrix:Matrix = new Matrix();
			var loseSprite:Sprite = new Sprite();
			loseMatrix.translate(game.cameraPosition, 0);
			loseSprite.graphics.beginBitmapFill(loseBmp.bitmapData, loseMatrix, true);
			loseSprite.graphics.drawRect(game.cameraPosition, 0, loseBmp.width, loseBmp.height);
			addChild(loseSprite);
		}
		
		internal function YouWinUI():void
		{
			var winBmp:Bitmap = new winImage();
			var winMatrix:Matrix = new Matrix();
			var winSprite:Sprite = new Sprite();
			winMatrix.translate(game.cameraPosition + winBmp.width - 512, 0);
			winSprite.graphics.beginBitmapFill(winBmp.bitmapData, winMatrix, true);
			winSprite.graphics.drawRect(game.cameraPosition + winBmp.width - 512, 0, winBmp.width, winBmp.height);
			addChild(winSprite);
			
			//Back Button
			var backButton:SimpleButton = new SimpleButton();
			var backBmp:Bitmap = new backImage();
			var backMatrix:Matrix = new Matrix();
			var backSprite:Sprite = new Sprite();
			backMatrix.translate(412, 600);
			backSprite.graphics.beginBitmapFill(backBmp.bitmapData, backMatrix, true);
			backSprite.graphics.drawRect(412, 600, backBmp.width, backBmp.height);
			backButton.overState = backButton.downState = backButton.upState = backButton.hitTestState = backSprite;
			backButton.addEventListener(MouseEvent.CLICK, MenuUI);
			addChild(backButton);
		}
		
		private function ResearchUI(event:MouseEvent):void
		{
			researchToggle = !researchToggle;
		}
	
	}

}