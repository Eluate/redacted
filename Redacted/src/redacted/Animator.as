package redacted
{
	import flash.display.Bitmap;
	import flash.utils.getTimer;
	
	/**
	 * This class is used for retriving animations and setting animations via code.
	 * Interval is the seconds between an image switch.
	 * Images is the array that holds the animation images.
	 * @author Harold Seefeld
	 */
	public class Animator
	{
		internal var images:Array = new Array();
		internal var interval:Number = 1;
		internal var initialTime:Number = 0;
		
		public function GetAnimationImage():Bitmap
		{
			var timePassed:Number = (getTimer() - initialTime) / 1000;
			if (timePassed > images.length * interval)
			{
				initialTime = getTimer();
			}
			for (var i:int = 0; i < images.length; i++)
			{
				if (timePassed < (i + 1) * interval)
				{
					return new images[i]() as Bitmap;
				}
			}
			return new images[0]() as Bitmap;
		}
		
		public function AddImage(image:Class):void
		{
			images.push(image);
		}
		
		public function Clone():Animator
		{
			var animator:Animator = new Animator();
			animator.interval = interval;
			animator.images = images;
			return animator;
		}
	}
}