package redacted
{
	import flash.display.Sprite;
	
	/**
	 * Contains the data that a unit should have when creating one in units.as
	 * @author Sausage Roll Studios
	 */
	public class Unit
	{
		// How much damage a unit does
		internal var unitDMG:int;
		// Index of the unit in the units array
		internal var unitID:int;
		// The range in which a unit can attack another
		internal var unitRNG:Number;
		// The name of the unit
		internal var unitName:String;
		// The units maximum hitpoints
		internal var unitMaxHP:int;
		// The units movement speed
		internal var unitSPD:Number;
		// The units attack speed (expressed as seconds)
		internal var unitCD:Number;
		// The gold cost of the unit
		internal var unitCost:int;
		// How long it takes to build the unit
		internal var unitBuildTime:Number;
		// The icon of the unit
		internal var unitIcon:Class;
		// The type id of the unit (used for upgrades)
		internal var unitType:int;
		// Cost to unlock unit
		internal var unitXPCost:int;
		// Is the unit locked
		internal var isLocked:Boolean;
		// Animations
		internal var moveAnimator:Animator;
		internal var attackAnimator:Animator;
	}

}