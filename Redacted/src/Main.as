package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import redacted.Game;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.system.Capabilities;
	import flash.display.Bitmap;
	import flash.geom.Matrix;
	import flash.utils.setTimeout;
	
	/**
	 * Stage initializer
	 * @author Harold Seefeld
	 */
	public class Main extends Sprite 
	{
		internal var game:Game;
		[Embed(source = "redacted/ui/splash.jpg")]
		private static var splashImage:Class;
		
		public function Main():void
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			stage.fullScreenSourceRect = new Rectangle(0, 0, Capabilities.screenResolutionX / 1.8, Capabilities.screenResolutionY / 1.8);
			stage.displayState = StageDisplayState.FULL_SCREEN;
			scaleX = Capabilities.screenResolutionX / 1.8 / 1024;
			scaleY = Capabilities.screenResolutionY / 1.8 / 720;
			// Render splash screen
			var splashBmp:Bitmap = new splashImage();
			var splashMatrix:Matrix = new Matrix();
			var splashSprite:Sprite = new Sprite();
			splashMatrix.translate(0, 0);
			splashSprite.graphics.beginBitmapFill(splashBmp.bitmapData, splashMatrix, true);
			splashSprite.graphics.drawRect(0, 0, splashBmp.width, splashBmp.height);
			addChild(splashSprite);
			setTimeout(function():void {
				// Entry Point
				game = new Game();
				game.Start();
				// Allow game to be rendered
				addChild(game);
			}, 3000);
		}
		
	}
	
}